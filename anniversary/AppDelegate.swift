//
//  AppDelegate.swift
//  anniversary
//
//  Created by zjf on 08/22/2014.
//  Copyright (c) 2014 zjf. All rights reserved.
//

import Cocoa

class AppDelegate: NSObject, NSApplicationDelegate {
    let dateStringFormatter = NSDateFormatter()
                            
    @IBOutlet weak var window: NSWindow!
    @IBOutlet weak var datepicker1 : NSDatePicker!
    @IBOutlet weak var datepicker2 : NSDatePicker!
    @IBOutlet weak var textField : NSTextField!
    @IBOutlet weak var calButton : NSButton!
    @IBOutlet weak var distanceTextField: NSTextField!
    @IBOutlet weak var distanceSlider : NSSlider!

    func applicationDidFinishLaunching(aNotification: NSNotification?) {
        dateStringFormatter.dateFormat = "yyyy-MM-dd EEEE"
        
        datepicker1.dateValue = NSDate()
        datepicker2.dateValue = NSDate()
        
    }

    func applicationWillTerminate(aNotification: NSNotification?) {
        // Insert code here to tear down your application
    }

    
    @IBAction func calcAction(sender: AnyObject) {
        var date: NSDate = datepicker1.dateValue;
        var datestr: String = dateStringFormatter.stringFromDate(date)
        var now: NSDate = NSDate()
        
        var interval:NSTimeInterval = now.timeIntervalSinceDate(date) / (24*3600);
        var cnt:Int = Int(interval)
        cnt++;
        let str:String = "到今天已经\(cnt)天。\n第一天从\(datestr)算起。"
        printlnTo(textField, str)
        println(str)
    }

    @IBAction func datepicker1Select(sender: AnyObject) {
        var date: NSDate = datepicker1.dateValue;
        datepicker2.dateValue = date;
    }
    
    @IBAction func datepicker2Select(sender: AnyObject) {
        var date: NSDate = datepicker2.dateValue;
        datepicker1.dateValue = date;
    }
    
    @IBAction func distanceSliderSelect(sender: AnyObject) {
        var cnt: Int = Int(distanceSlider.doubleValue)
        distanceTextField.stringValue = "\(cnt)"
    }
    
    @IBAction func distanceTextFieldSelect(sender: AnyObject) {
        var cntstr: NSString = distanceTextField.stringValue.stringByReplacingOccurrencesOfString(",", withString: "")
        var val : Int32 = cntstr.intValue
        distanceSlider.doubleValue = Double(val)
    }
    
    @IBAction func toDateClick(sender: AnyObject) {
        var date: NSDate = datepicker1.dateValue;
        var datestr:String = dateStringFormatter.stringFromDate(date)
        var cntstr: NSString = distanceTextField.stringValue.stringByReplacingOccurrencesOfString(",", withString: "")
        var interval: Int32 = cntstr.intValue
        if interval == 0 {
            return;
        }
        var timeinter: NSTimeInterval = NSTimeInterval((interval - 1)*24*3600)
        var todate: NSDate = date.dateByAddingTimeInterval(timeinter)
        var todatestr: String = dateStringFormatter.stringFromDate(todate)
        
        let str:String = "第\(interval)天后的那一天为\(todatestr)。\n第一天从\(datestr)算起"
        printlnTo(textField, str)
        println(str)
    }
    
}

