//
//  UITools.swift
//  anniversary
//
//  Created by zjf on 08/22/2014.
//  Copyright (c) 2014 zjf. All rights reserved.
//

import Cocoa

func printlnTo(scrollView:NSScrollView!, text:String!, title:String?=nil){
    println("title: \(title?)\ntext: \(text)")
    
    var txtview : NSTextView? = scrollView.documentView as? NSTextView
    
    var attr: NSAttributedString = NSAttributedString(string:"\(text)\n")
    
    objc_sync_enter(scrollView)
    if title != nil {
        var attrTitle: NSAttributedString = NSAttributedString(string:"\(title)\n")
        txtview?.textStorage.appendAttributedString(attrTitle)
    }
    txtview?.textStorage.appendAttributedString(attr)
    objc_sync_exit(scrollView)
    
}

func printlnTo(textField:NSTextField!, text:String!, title:String?=nil){
    println("title: \(title?)\ntext: \(text)")
    
    objc_sync_enter(textField)
    if title == nil {
        textField.stringValue = text
    }else{
        textField.stringValue = "\(title!)\n\(text)"
    }
    objc_sync_exit(textField)
    
}

